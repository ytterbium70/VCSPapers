<https://arxiv.org/abs/1407.3561>
=================================

[pdf/1407.3561](pdf/1407.3561.pdf)

-   IPFS is a distributed file system which synthesizes suc- cessful
    ideas from previous peer-to-peer sytems, including DHTs, BitTorrent,
    Git, and SFS.
-   3.6.4 File Object: commit - The commit object in IPFS represents a
    snapshot in the version history of any object. It is similar to
    Git's, but can reference any type of object.

<https://arxiv.org/abs/1601.02433>
==================================

[pdf/1601.02433](pdf/1601.02433.pdf)

-   Git-based Versioning for Collaborative Vocabulary Development
-   Basic pros / cons
-   Interface on top of git

<https://arxiv.org/abs/1612.05028>
==================================

[pdf/1612.05028](pdf/1612.05028.pdf)

-   Ontohub: A semantic repository for heterogeneous ontologies
-   Advanced (mathematical) dependency checking
-   Uses git for versioned storage

<https://arxiv.org/abs/1701.05681>
==================================

[pdf/1701.05681](pdf/1701.05681.pdf)

-   Stylistic Authorship Attribution of Small, Incomplete Source Code
    Fragments
-   de-anonymization / authorship attribution of public code
-   For ground truth we use git blame to assign author- ship to
    individual lines of code. Git blame is a heuristic which attributes
    code to the last author to modify that code. Meng et al. proposed a
    tool called git-author which assigns weighted values to
    contributions in order to bet- ter represent the evolution of a line
    of code \[21\]. This tool creates a repository graph with commits as
    nodes and development dependencies as edges, and then de- fines
    structural authorship and weighted authorship for each line of code,
    where structural authorship is a sub- graph of the repository graph
    with respect to that line of code and weighted authorship is a
    vector of programmer contributions derived from structural
    authorship.
-   Xiaozhu Meng, Barton P Miller, William R Williams, and Andrew R
    Bernat. 2013. Mining software repositories for accurate authorship.
    In 2013 IEEE International Conference on Software Maintenance. IEEE,
    250--259.

<https://arxiv.org/abs/1703.09603>
==================================

[pdf/1703.09603](pdf/1703.09603.pdf)

-   Towards Automatic Generation of Short Summaries of Commits
-   automatic commit message generation using NLP + ML

<https://arxiv.org/abs/1708.09492>
==================================

[pdf/1708.09492](pdf/1708.09492.pdf)

-   Automatically Generating Commit Messages from Diffs using Neural
    Machine Translation
-   also involves QA

<https://arxiv.org/abs/1803.10144>
==================================

[pdf/1803.10144](pdf/1803.10144.pdf)

-   Public Git Archive: a Big Code dataset for all
-   We describe the data retrieval pipeline which places forks into the
    original repository without mixing identities by reusing the
    existing Git objects \[5\].
-   Marco Biazzini, Martin Monperrus, and Benoit Baudry. 2014. On
    Analyzing the Topology of Commit Histories in Decentralized Version
    Control Systems. In ICSME. IEEE, 261--270.

<https://arxiv.org/abs/1805.03721>
==================================

[pdf/1805.03721](pdf/1805.03721.pdf)

-   Decentralized Collaborative Knowledge Management using Git
-   Extending git to handle advanced dependency checks with RDF vocab
    support, improved from Git4Voc

<https://arxiv.org/abs/1810.08532>
==================================

[pdf/1810.08532](pdf/1810.08532.pdf)

-   Coming: a Tool for Mining Change Pattern Instances from Git Commits

<https://arxiv.org/abs/1811.02021>
==================================

[pdf/1811.02021](pdf/1811.02021.pdf)

-   Using GitHub Classroom To Teach Statistics
-   accessibility, usage, etc.

<https://arxiv.org/abs/1811.07643>
==================================

[pdf/1811.07643](pdf/1811.07643.pdf)

-   Understanding Repository Starring Practices in a Social Coding
    Platform
-   Web scraping + public dataset + website for github repo popularity /
    stars

<https://arxiv.org/abs/1902.02467>
==================================

[pdf/1902.02467](pdf/1902.02467.pdf)

-   How Different Are Different diff Algorithms in Git?

<https://arxiv.org/abs/1904.06584>
==================================

[pdf/1904.06584](pdf/1904.06584.pdf)

-   GoT: Git, but for Objects
-   impractical git-like system for objects under a changing application

<https://arxiv.org/abs/1911.07988>
==================================

[pdf/1911.07988](pdf/1911.07988.pdf)

-   Invariant Diffs
-   diffs not for code, but for program conditions.

<https://arxiv.org/pdf/1911.11690.pdf>
======================================

[pdf/11690.pdf](pdf/11690.pdf.pdf)

-   Generating Commit Messages from Git Diffs
-   similar to previous similar papers

[bieniusa2008 10.1109 CSSE.2008.1079.pdf](pdf/bieniusa2008 10.1109 CSSE.2008.1079.pdf)
=======================================

-   The Relation of Version Control to Concurrent Programming

[documentation-meets-version-control-an-automated-backup-system-f 10.1109 IPCC.2000.887311.pdf](pdf/documentation-meets-version-control-an-automated-backup-system-f 10.1109 IPCC.2000.887311.pdf)
=============================================================================================

-   Documentation Meets Version Control: An Automated Backup System for
    HTML-Based Help
-   niche VCS implementation

[fomin2009 10.1109 CEE-SECR.2009.5501149.pdf](pdf/fomin2009 10.1109 CEE-SECR.2009.5501149.pdf)
===========================================

-   "The Cathedral or The Bazaar": Version Control --- Centralized or
    Distributed?
-   What approach is the best choice for corporate software development?
    We\'ll discuss the basic \"rubs\" in the argument between
    centralized and distributed VCS\'s, will analyse their advantages
    and drawbacks in the context of corporate development, and, finally,
    will propose how to combine the best from both approaches and avoid
    their inherent problems.
-   russian :(

[ghezzi2012 10.1109 TOPI.2012.6229803.pdf](pdf/ghezzi2012 10.1109 TOPI.2012.6229803.pdf)
========================================

-   An Architectural Blueprint for a Pluggable Version Control System
    for Software (Evolution) Analysis
-   we propose an architectural blueprint for a plug-in based version
    control system in which analyses can be directly plugged into it in
    a flexible and lightweight way, to support both developers and
    analysts.

[hinsen2009 10.1109 MCSE.2009.194.pdf](pdf/hinsen2009 10.1109 MCSE.2009.194.pdf)
====================================

-   almost an introductory article to VCS

[jenkins2014 10.1049 cp.2014.0152.pdf](pdf/jenkins2014 10.1049 cp.2014.0152.pdf)
====================================

-   Version Control and Patch Management of Protection and Automation
    Systems
-   lateral implementation for physical systems

[kasimov2010 10.1109 ICAICT.2010.5612028.pdf](pdf/kasimov2010 10.1109 ICAICT.2010.5612028.pdf)
===========================================

-   Using version control systems in software development
-   not very detailed
-   russian!

[koc201210.1109 ASONAM.2012.129.pdf](pdf/koc201210.1109 ASONAM.2012.129.pdf)
==================================

-   Towards Social Version Control
-   proposal for distributed, non-private version control

[li2016 10.1109 WAINA.2016.107.pdf](pdf/li2016 10.1109 WAINA.2016.107.pdf)
=================================

-   Analysis of Software Developer Activity on a Distributed Version
    Control System
-   github analytics

[majumdar2017 10.1109 ICRITO.2017.8342438.pdf](pdf/majumdar2017 10.1109 ICRITO.2017.8342438.pdf)
============================================

-   Source Code Management Using Version Control System
-   shallow paper

[rao2015 10.1109 ICRITO.2015.7359255.pdf](pdf/rao2015 10.1109 ICRITO.2015.7359255.pdf)
=======================================

-   Embedding Version Tag in Software File Deliverables before Build
    Release
-   Minor edits to commit structure

[rodriguez-bustos2012 10.1109 MSR.2012.6224297.pdf](pdf/rodriguez-bustos2012 10.1109 MSR.2012.6224297.pdf)
=================================================

-   How Distributed Version Control Systems Impact Open Source Software
    Projects
-   Stats on Mozilla

[saito2017 10.1109 RE.2017.33.pdf](pdf/saito2017 10.1109 RE.2017.33.pdf)
================================

-   How Much Undocumented Knowledge is there in Agile Software
    Development? Case Study on Industrial Project using Issue Tracking
    System and Version Control System

[schroeder2012 10.1109 CTS.2012.6261115.pdf](pdf/schroeder2012 10.1109 CTS.2012.6261115.pdf)
==========================================

-   GitOD: an On Demand Distributed File System Approach to Version
    Control
-   Since objects are connected through a known structure of snapshots
    and files, it is possible to predict objects that might be needed in
    the near future based on objects that have been recently retrieved.
    This prediction could allow the file system to proactively download
    files that have a high probability of being requested by the user.

[schroeder dissertation 10.1109 CTS.2012.6261115.pdf](pdf/schroeder dissertation 10.1109 CTS.2012.6261115.pdf)
===================================================

-   dissertation of the above

[vrcs-integrating-version-control-and-module-management-using-int 10.1109 VL.1997.626577.pdf](pdf/vrcs-integrating-version-control-and-module-management-using-int 10.1109 VL.1997.626577.pdf)
===========================================================================================

-   VRCS: Integrating Version Control and Module Management using
    Interactive Three-Dimensional Graphics
-   3D visualization for multiple interconnected components

[xu2019 10.1109 ICME.2019.00138.pdf](pdf/xu2019 10.1109 ICME.2019.00138.pdf)
==================================

-   enforcing access control in distributed version control systems
-   node access control via Attribute-based Encryption (ABE) and
    Attribute- based Signature (ABS)
